import 'package:makeinindiatrade/src/API%20Provider/category.dart';
import 'package:makeinindiatrade/src/API%20Provider/login.dart';
import 'package:makeinindiatrade/src/model/category_model.dart';
import 'package:makeinindiatrade/src/model/login_model.dart';

class Repository {
  Future<LoginModel> loginApiRepository(String email, String password) async {
    return await LoginApi().loginApi(email, password);
  }

  Future<CategoryModel> getCategoryRepository() async {
    return await CategoryApi().categoryApi();
  }
}
