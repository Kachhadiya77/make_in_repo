import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:makeinindiatrade/src/Images/images.dart';
import 'package:shared_preferences/shared_preferences.dart';

// Color of application.
class AppColor {
  static const Color appColor = Color(0XFF6621F9);
}

//App logo
class AppLogo extends StatelessWidget {
  const AppLogo({this.size});
  final double size;

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      Images.LOGO,
      height: size ?? 150,
      width: size ?? 150,
    );
  }
}

int selectedMenu;
SharedPreferences preferences;

// Custom TextField
class CustomTextField extends StatelessWidget {
  const CustomTextField(
      {this.onTap,
      this.controller,
      this.focusNode,
      this.errorText,
      this.onSave,
      this.hintText,
      this.validator,
      this.labelText,
      this.onSubmitted,
      this.readOnly,
      this.textInputAction,
      this.isSuffix,
      this.obscureText,
      this.suffixIcon,
      this.maxLength,
      this.keyboardType,
      this.onChanged});

  final Function onTap;
  final TextEditingController controller;
  final FocusNode focusNode;
  final bool readOnly;
  final Function(String) onChanged;
  final Function(String) onSubmitted;
  final Function(String) validator;
  final Function(String) onSave;
  final String labelText;
  final String hintText;
  final int maxLength;
  final bool isSuffix;
  final String errorText;
  final Widget suffixIcon;
  final bool obscureText;
  final TextInputAction textInputAction;
  final TextInputType keyboardType;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: TextFormField(
        onTap: onTap,
        onFieldSubmitted: onSubmitted,
        validator: validator,
        controller: controller,
        autofocus: false,
        obscureText: obscureText ?? false,
        keyboardType: keyboardType,
        focusNode: focusNode,
        onSaved: onSave,
        maxLength: maxLength,
        onChanged: onChanged,
        readOnly: readOnly ?? false,
        textInputAction: textInputAction,
        decoration: InputDecoration(
          errorText: errorText,
          suffixIcon: isSuffix ?? false ? suffixIcon : null,
          border: OutlineInputBorder(
            borderSide: BorderSide(color: AppColor.appColor, width: 1.0),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[400], width: 1.0),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: AppColor.appColor, width: 1.0),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 0.9),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 0.9),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          hintText: hintText,
          labelStyle: TextStyle(
              color: Color.fromRGBO(156, 156, 156, 1), fontSize: 16.0),
          helperStyle: TextStyle(color: Colors.red),
          counterStyle: TextStyle(color: Colors.black),
          labelText: labelText,
        ),
      ),
    );
  }
}

class CustomButton extends StatelessWidget {
  const CustomButton(
      {this.height,
      this.title,
      this.onPressed,
      this.isRoundBorder,
      this.width});
  final double height;
  final double width;

  final String title;
  final Function onPressed;
  final bool isRoundBorder;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height ?? 55,
      width: width ?? 150,
      child: RaisedButton(
        child: Text(
          title.toUpperCase(),
          style: TextStyle(color: Colors.white, fontSize: 16.5),
        ),
        onPressed: onPressed,
        color: AppColor.appColor,
        disabledColor: AppColor.appColor.withOpacity(0.5),
        shape: isRoundBorder ?? false
            ? RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              )
            : RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
      ),
    );
  }
}

class SingleLineTextField extends StatelessWidget {
  const SingleLineTextField({
    this.hintText,
    this.controller,
    this.enabledValue,
    this.onTapped,
    this.textInputType,
    this.helperText = '',
    this.errortxt,
    this.contentPadding,
    this.lbltext,
    this.node,
    this.onSubmitted,
    this.obscureText,
    this.maxline,
    this.onChange,
    this.suffix,
    this.suffixIcon,
  });
  final FocusNode node;
  final String errortxt;
  final String hintText;
  final String lbltext;
  final dynamic controller;
  final bool enabledValue;
  final Function onTapped;
  final TextInputType textInputType;
  final String helperText;
  final EdgeInsets contentPadding;
  final Function(String) onSubmitted;
  final bool obscureText;
  final int maxline;
  final Function(String) onChange;
  final Widget suffix;
  final Widget suffixIcon;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SizedBox(
        child: TextField(
          onChanged: onChange,
          focusNode: node,
          onSubmitted: onSubmitted,
          cursorColor: AppColor.appColor,
          textCapitalization: TextCapitalization.sentences,
          enabled: enabledValue,
          maxLines: 1,
          onTap: onTapped,
          obscureText: obscureText == null ? false : obscureText ?? false,
          controller: controller,
          textAlign: TextAlign.left,
          keyboardType: textInputType,
          decoration: InputDecoration(
            suffix: suffix,
            suffixIcon: suffixIcon,
            contentPadding: contentPadding,
            labelStyle: TextStyle(
              height: 1,
              //  fontFamily: 'PoppinsRegular',
              color: Colors.grey,
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: AppColor.appColor,
              ),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black, width: 0.6),
            ),
            hintText: hintText,
            labelText: lbltext,
            hintStyle: TextStyle(
              fontSize: 13,
              fontWeight: FontWeight.w300,
              color: AppColor.appColor,
            ),
            helperText: helperText.isEmpty ? null : helperText,
            helperStyle: TextStyle(
              //  fontFamily: 'PoppinsRegular',
              color: Colors.grey,
            ),
            errorText: errortxt,
          ),
          style: TextStyle(
            //  fontFamily: 'PoppinsRegular',
            color: Colors.grey,
          ),
        ),
      ),
    );
  }
}

class Indicators {
  Widget indicatorWidget(
      {double size = 20.0, Color color, bool addWaitText = false}) {
    final Widget indicator = SpinKitFadingCircle(
      color: color ?? AppColor.appColor,
      size: size,
    );
    return indicator;
  }

  void indicatorPopupWillNotPop(BuildContext context,
      {bool barrierDismissible = false}) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () {
            return null;
          },
          child: AlertDialog(
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(8.0),
              ),
            ),
            content: Padding(
              padding: const EdgeInsets.only(top: 16.0, bottom: 8.0),
              child: SpinKitFadingCircle(
                color: AppColor.appColor,
                size: 35.0,
              ),
            ),
          ),
        );
      },
    );
  }

  void hideIndicator(BuildContext context) {
    return Navigator.pop(context);
  }
}
