class Strings {
  //sign_in_screen
  static const String emailID = 'Email ID';
  static const String password = 'Password';
  static const String forgot = 'Forgot?';
  static const String signIn = 'Sign In';
  static const String newUser = 'New User?';
  static const String signUp = 'Sign Up';
  static const String validateEmail = 'Please enter valid email adddress.';
  static const String validatePassword = 'Please enter valid password.';

  //sign_up_screen
  static const String firstname = 'First Name';
  static const String lastname = 'Last Name';
  static const String phoneNumber = 'Phone Number';
  static const String city = 'City';
  static const String pincode = 'Pincode';
  static const String confirmPassword = 'Confirm-Password';
  static const String existingUser = 'Existing User?';
  static const String validateName = 'Please enter valid name.';
  static const String validatePhone = 'Please enter valid number.';
  static const String validatePincode = 'Please enter valid pincode.';
  static const String passwordNotMatch =
      'Password is not matched with above password.';

  //forgot_password_screen
  static const String getPassword = 'Get Password';
  static const String back = 'Back';

  //select your city
  static const String selectyourlocation = 'Select Your Location';
  static const String yourselectedLocationis = 'Your Selected Location is';
  static const String exploreMoreCity = 'Explore More Cities';
//Home Screen

static const String makeInIndia = 'MAKEININDIATRADE';

//Drower
static const String buyer = 'Buyer';
static const String seller = 'Seller';


//Setting
static const String setting = 'Setting';

//Favourite
static const String favourite = 'Favourite';


}
