class CategoryModel {
  CategoryModel({this.flag, this.message, this.data});

  CategoryModel.fromJson(Map<String, dynamic> json) {
    flag = json['flag'];
    message = json['message'];
    data = json['data'] != null ? AllData.fromJson(json['data']) : null;
  }
  bool flag;
  String message;
  AllData data;

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['flag'] = flag;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class AllData {
  AllData({this.data, this.advertise});

  AllData.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <CategoryData>[];
      json['data'].forEach((v) {
        data.add(CategoryData.fromJson(v));
      });
    }
    advertise = json['advertise'];
  }

  List<CategoryData> data;
  dynamic advertise;

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['advertise'] = advertise;
    return data;
  }
}

class CategoryData {
  CategoryData(
      {this.sId,
      this.id,
      this.name,
      this.parentCategoryId,
      this.image,
      this.sequenceNo,
      this.slug,
      this.subcategories});

  CategoryData.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    id = json['id'];
    name = json['name'];
    parentCategoryId = json['parent_category_id'];
    image = json['image'];
    sequenceNo = json['sequence_no'];
    slug = json['slug'];
    if (json['subcategories'] != null) {
      subcategories = <Subcategories>[];
      json['subcategories'].forEach((v) {
        subcategories.add(Subcategories.fromJson(v));
      });
    }
  }

  String sId;
  String id;
  String name;
  dynamic parentCategoryId;
  String image;
  int sequenceNo;
  String slug;
  List<Subcategories> subcategories;

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['_id'] = sId;
    data['id'] = id;
    data['name'] = name;
    data['parent_category_id'] = parentCategoryId;
    data['image'] = image;
    data['sequence_no'] = sequenceNo;
    data['slug'] = slug;
    if (subcategories != null) {
      data['subcategories'] = subcategories.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Subcategories {
  Subcategories(
      {this.sId, this.id, this.name, this.parentCategoryId, this.sequenceNo});

  Subcategories.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    id = json['id'];
    name = json['name'];
    parentCategoryId = json['parent_category_id'] !=  null ? json['parent_category_id'] : '';
    sequenceNo = json['sequence_no'];
  }

  String sId;
  String id;
  String name;
  String parentCategoryId;
  int sequenceNo;

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['_id'] = sId;
    data['id'] = id;
    data['name'] = name;
    data['parent_category_id'] = parentCategoryId;
    data['sequence_no'] = sequenceNo;
    return data;
  }
}
