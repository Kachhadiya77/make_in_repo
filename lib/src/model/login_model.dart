class LoginModel {
  LoginModel({this.flag, this.message, this.data});

  bool flag;
  String message;
  Data data;

  // ignore: sort_constructors_first
  LoginModel.fromJson(Map<String, dynamic> json) {
    flag = json['flag'];
    message = json['message'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['flag'] = flag;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  Data(
      {this.sId,
      this.id,
      this.firstName,
      this.lastName,
      this.email,
      this.mobile,
      this.cityId,
      this.pincode,
      this.isActive,
      this.isVerifiedEmail,
      this.isVerifiedMobile,
      this.isMigrated,
      this.rawPassword,
      this.basicDetail,
      this.category,
      this.phone,
      this.phone2,
      this.mobile2,
      this.email2,
      this.profileUrl,
      this.originalUrl,
      this.profileSource,
      this.updatedAt,
      this.createdAt,
      this.companyName,
      this.hasUrl,
      this.slug});

  String sId;
  String id;
  String firstName;
  String lastName;
  String email;
  String mobile;
  String cityId;
  String pincode;
  int isActive;
  int isVerifiedEmail;
  int isVerifiedMobile;
  int isMigrated;
  String rawPassword;
  String basicDetail;
  String category;
  String phone;
  String phone2;
  String mobile2;
  String email2;
  String profileUrl;
  String originalUrl;
  String profileSource;
  String updatedAt;
  String createdAt;
  String companyName;
  int hasUrl;
  String slug;

  // ignore: sort_constructors_first
  Data.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    mobile = json['mobile'];
    cityId = json['city_id'];
    pincode = json['pincode'];
    isActive = json['is_active'];
    isVerifiedEmail = json['is_verified_email'];
    isVerifiedMobile = json['is_verified_mobile'];
    isMigrated = json['is_migrated'];
    rawPassword = json['raw_password'];
    basicDetail = json['basic_detail'];
    category = json['category'];
    phone = json['phone'];
    phone2 = json['phone2'];
    mobile2 = json['mobile2'];
    email2 = json['email2'];
    profileUrl = json['profile_url'];
    originalUrl = json['original_url'];
    profileSource = json['profile_source'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    companyName = json['company_name'] == null ? '' : json['company_name'];
    hasUrl = json['has_url'];
    slug = json['slug'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['_id'] = sId;
    data['id'] = id;
    data['first_name'] = firstName;
    data['last_name'] = lastName;
    data['email'] = email;
    data['mobile'] = mobile;
    data['city_id'] = cityId;
    data['pincode'] = pincode;
    data['is_active'] = isActive;
    data['is_verified_email'] = isVerifiedEmail;
    data['is_verified_mobile'] = isVerifiedMobile;
    data['is_migrated'] = isMigrated;
    data['raw_password'] = rawPassword;
    data['basic_detail'] = basicDetail;
    data['category'] = category;
    data['phone'] = phone;
    data['phone2'] = phone2;
    data['mobile2'] = mobile2;
    data['email2'] = email2;
    data['profile_url'] = profileUrl;
    data['original_url'] = originalUrl;
    data['profile_source'] = profileSource;
    data['updated_at'] = updatedAt;
    data['created_at'] = createdAt;
    data['company_name'] = companyName;
    data['has_url'] = hasUrl;
    data['slug'] = slug;
    return data;
  }
}
