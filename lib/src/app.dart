import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:makeinindiatrade/src/bloc/auto_validate_bloc.dart';
import 'package:makeinindiatrade/src/bloc/category_bloc.dart';
import 'package:makeinindiatrade/src/bloc/favouriteBloc.dart';
import 'package:makeinindiatrade/src/bloc/login_bloc.dart';
import 'package:makeinindiatrade/src/bloc/password_visibility_bloc.dart';
import 'package:makeinindiatrade/src/bloc/validation.dart';
import 'package:makeinindiatrade/src/constant/constant.dart';
import 'package:makeinindiatrade/src/model/category_model.dart';
import 'package:makeinindiatrade/src/model/login_model.dart';
import 'package:makeinindiatrade/src/shared_preference/prefs_keys.dart';
import 'package:makeinindiatrade/src/view/home.dart';
import 'package:makeinindiatrade/src/view/sign_in_screen.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'bloc/citybloc.dart';

class MyApp extends StatelessWidget {
  MyApp(this.prefs);
  final SharedPreferences prefs;
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: _providers,
      child: MaterialApp(
        title: 'Make in india',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          fontFamily: 'Objectivity',
          primaryColor: Color(0XFF6636FF),
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: _dicedeScreen(),
      ),
    );
  }

  Widget _dicedeScreen() {
    final bool user = prefs.getBool(PrefKeys.IS_LOGIN);
    preferences = prefs;
    if (user == null) {
      return SignInScreen();
    } else {
      return HomePage();
    }
  }

  final List<SingleChildWidget> _providers = [
    Provider<PasswordVisibilityBloc>(
      create: (_) => PasswordVisibilityBloc(),
    ),
    Provider<VisiblePassBloc>(
      create: (_) => VisiblePassBloc(),
    ),
    Provider<VisibleConfPassBloc>(
      create: (_) => VisibleConfPassBloc(),
    ),
    Provider<CityListBloc>(
      create: (_) => CityListBloc(),
    ),
    Provider<ValidationBloc>(
      create: (_) => ValidationBloc(),
    ),
    Provider<LoginBloc>(
      create: (_) => LoginBloc(),
    ),
    Provider<AutoValidate>(
      create: (_) => AutoValidate(),
    ),
    Provider<FavouriteBloc>(
      create: (_) => FavouriteBloc(),
    ),
    Provider<CategoryBloc>(
      create: (_) => CategoryBloc(),
    ),
  ];
}
