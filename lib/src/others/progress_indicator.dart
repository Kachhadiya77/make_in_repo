import 'package:flutter/material.dart';
import 'package:makeinindiatrade/src/constant/constant.dart';
import 'package:makeinindiatrade/src/constant/constant.dart';

class MProgressIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _mainCircle(),
        _stightLineOne(),
        _emptyCircleOne(),
        _stightLineTwo(),
        _emptyCircleTwo(),
      ],
    );
  }

  Widget _mainCircle() {
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      curve: Curves.fastOutSlowIn,
      height: 18,
      width: 18,
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
        border: Border.all(
          color: AppColor.appColor,
        ),
      ),
      child: Container(
        margin: EdgeInsets.all(2.0),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: AppColor.appColor,
        ),
      ),
    );
  }

  Widget _stightLineOne() {
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      height: 2,
      width: 50,
      color: HandleIndicator.colorLineOne,
      curve: Curves.fastOutSlowIn,
    );
  }

  Widget _emptyCircleOne() {
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      height: HandleIndicator.smallSizeOne,
      width: HandleIndicator.smallSizeOne,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          color: HandleIndicator.borderColorOne,
        ),
      ),
      child: AnimatedContainer(
        duration: Duration(milliseconds: 300),
        margin: EdgeInsets.all(HandleIndicator.marginOne),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: HandleIndicator.colorInOne,
        ),
      ),
    );
  }

  Widget _stightLineTwo() {
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      height: 2,
      width: 50,
      color: HandleIndicator.colorLineTwo,
      curve: Curves.fastOutSlowIn,
    );
  }

  Widget _emptyCircleTwo() {
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      height: HandleIndicator.smallSizeTwo,
      width: HandleIndicator.smallSizeTwo,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          color: HandleIndicator.borderColorTwo,
        ),
      ),
      child: AnimatedContainer(
        duration: Duration(milliseconds: 300),
        margin: EdgeInsets.all(HandleIndicator.marginTwo),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: HandleIndicator.colorInTwo,
        ),
      ),
    );
  }
}

// ignore: avoid_classes_with_only_static_members
class HandleIndicator {
  static var anInteger = 0;

  static Color colorLineOne = Colors.grey[400];

  // Stright Line Two
  static Color colorLineTwo = Colors.grey[400];

  // first empty circle
  static double smallSizeOne = 10.0;
  static Color borderColorOne = Colors.grey[500];
  static double marginOne = 0.0;
  static Color colorInOne = Colors.white;

  // Second empty circle
  static double smallSizeTwo = 10.0;
  static Color borderColorTwo = Colors.grey[500];
  static double marginTwo = 0.0;
  static Color colorInTwo = Colors.white;

  // ignore: always_declare_return_types
  static changeInteger(int i) {
    if (i == 0) {
      colorLineOne = AppColor.appColor;
      smallSizeOne = 18.0;
      marginOne = 2.0;
      colorInOne = AppColor.appColor;
      i = 1;
    } else {
      colorLineTwo = AppColor.appColor;
      smallSizeTwo = 18.0;
      marginTwo = 2.0;
      colorInTwo = AppColor.appColor;
      i = 1;
    }
  }
}
