import 'dart:async';
import 'package:makeinindiatrade/src/constant/strings.dart';


class Validator {
  final numberValidate = StreamTransformer<String, String>.fromHandlers(
    handleData: (number, sink) {
      if (number.length == 10) {
        sink.add(number);
      } else {
        sink.addError(Strings.validateEmail);
      }
    },
  );

  final passwordValidate = StreamTransformer<String, String>.fromHandlers(
      handleData: (password, sink) {
    if (password.length > 1) {
      sink.add(password);
    } else {
      sink.addError(Strings.validatePassword);
    }
  });
}
