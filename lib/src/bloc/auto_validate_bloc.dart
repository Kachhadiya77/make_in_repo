import 'package:rxdart/rxdart.dart';

class AutoValidate {
  final _autoValidateController = BehaviorSubject<bool>();

  Stream<bool> get autoValidateStream => _autoValidateController.stream;

  void autoValidateSink(bool value) {
    _autoValidateController.sink.add(value);
  }

  void dispose() {
    _autoValidateController.close();
  }
}
