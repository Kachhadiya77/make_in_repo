import 'package:makeinindiatrade/src/model/category_model.dart';
import 'package:makeinindiatrade/src/repository/login_repository.dart';
import 'package:rxdart/rxdart.dart';

class CategoryBloc {
  final _categoryBlocController = PublishSubject<CategoryModel>();

  Stream<CategoryModel> get categoryStream => _categoryBlocController.stream;

  Future<CategoryModel> categorySink() async {
    return await Repository().getCategoryRepository();
  }

  void dispose() {
    _categoryBlocController.close();
  }
}
