import 'package:rxdart/rxdart.dart';

class FavouriteBloc {
  final favouriteController = BehaviorSubject<List>();

  List dataContain = [];
  Stream<List> get favouriteSTREAM => favouriteController.stream;


  void favouriteSINK(List value,) {
favouriteController.sink.add(value);
  }



  void dispose() {
    favouriteController.close();

  }
}
