import 'package:rxdart/rxdart.dart';

class CityListBloc {
  final _cityController = PublishSubject<List>();
  final _expandedlistController = BehaviorSubject<bool>();
  final _stringController = PublishSubject<String>();
  List dataContain = [];
  Stream<List> get citySTREAM => _cityController.stream;
  Stream<bool> get expandedSTREAM => _expandedlistController.stream;
  Stream<String> get stringSTREAM => _stringController.stream;

  void citySINK(List value, String text) {
    if (text.isEmpty) {
      _cityController.sink.add(value);
    } else {
      dataContain = [];
      for (var item in value) {
        if (item['state_name'].toString().toLowerCase().contains(text)) {
          dataContain.add(item);
        }
      }
      print(dataContain);
      _cityController.sink.add(dataContain);
    }
  }

  void expandedSINK(bool value) {
    _expandedlistController.sink.add(value);
  }

  void stringSINK(String value) {
    _stringController.sink.add(value);
  }

  void dispose() {
    _expandedlistController.close();
    _cityController.close();
    _stringController.close();
  }
}
