import 'package:rxdart/rxdart.dart';

class PasswordVisibilityBloc {
  final _visibilityController = BehaviorSubject<bool>();

  Stream<bool> get visibilityStream => _visibilityController.stream;

  void visibilitySink(bool value) {
    _visibilityController.sink.add(value);
  }

  void dispose() {
    _visibilityController.close();
  }
}

class VisiblePassBloc {
  final _visibilityController = BehaviorSubject<bool>();

  Stream<bool> get visibilityStream => _visibilityController.stream;

  void visibilitySink(bool value) {
    _visibilityController.sink.add(value);
  }

  void dispose() {
    _visibilityController.close();
  }
}

class VisibleConfPassBloc {
  final _visibilityController = BehaviorSubject<bool>();
  final _txtCityNameController = BehaviorSubject<String>();

  Stream<bool> get visibilityStream => _visibilityController.stream;
  Stream<String> get txtSTREAM => _txtCityNameController.stream;

  void visibilitySink(bool value) {
    _visibilityController.sink.add(value);
  }

  void txtSINK(String value) {
    _txtCityNameController.sink.add(value);
  }

  void dispose() {
    _visibilityController.close();
    _txtCityNameController.close();
  }

  
}
