import 'package:makeinindiatrade/src/model/login_model.dart';
import 'package:makeinindiatrade/src/repository/login_repository.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc {
  final _loginBlocController = PublishSubject<LoginModel>();

  Stream<LoginModel> get loginStream => _loginBlocController.stream;

  Future<LoginModel> loginSink(String email, String password) async {
    return await Repository().loginApiRepository(email, password);
  }

  void dispose() {
    _loginBlocController.close();
  }
}
