import 'package:makeinindiatrade/src/bloc/validator.dart';
import 'package:rxdart/rxdart.dart';

class ValidationBloc extends Object with Validator {
  /// This sectioon is for sign in...
  final _number = BehaviorSubject<String>();
  final _password = BehaviorSubject<String>();

  Stream<String> get number => _number.stream.transform(numberValidate);
  Stream<String> get password => _password.stream.transform(passwordValidate);

  Function(String) get changeNumber => _number.sink.add;
  Function(String) get changePassword => _password.sink.add;

  Stream<bool> get submitValid =>
      Rx.combineLatest2(number, password, (e, p) => true);

  void submit() {
    final validEmail = _number.value;
    final validPassword = _password.value;

    print('Email is $validEmail, and password is $validPassword');
  }

  void dispose() {
    _number.close();
    _password.close();
  }
}
