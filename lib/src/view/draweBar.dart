import 'dart:io';

import 'package:flutter/material.dart';
import 'package:makeinindiatrade/src/constant/constant.dart';
import 'package:makeinindiatrade/src/constant/strings.dart';
import 'package:makeinindiatrade/src/shared_preference/prefs_keys.dart';
import 'package:makeinindiatrade/src/view/favourite.dart';
import 'package:makeinindiatrade/src/view/setting.dart';
import 'package:makeinindiatrade/src/view/sign_in_screen.dart';

class DrawerBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: DrawerOnly());
  }
}

class DrawerOnly extends StatefulWidget {
  @override
  _DrawerOnlyState createState() => _DrawerOnlyState();
}

class _DrawerOnlyState extends State<DrawerOnly> {
//   @override
//   Widget build(BuildContext context) {
//     return Container(

//     );
//   }
// }

// class DrawerOnly extends StatelessWidget {
  File _image;
  String oppoURL;
  String src;
  Map<String, dynamic> dic = {};

  List<Widget> listOFcell = [];
  // bool selectedCell = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    listOFcell = [
      Container(
        color: selectedMenu == 0 ? Color(0xffECE4FE) : Colors.transparent,
        height: 45,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: 20,
            ),
            Image.asset(
              selectedMenu == 0
                  ? 'assets/images/ic_home_selected.png'
                  : 'assets/images/ic_home_unselected.png',
              height: 20,
              width: 20,
            ),
            SizedBox(
              width: 20,
            ),
            Text(
              'Home',
              style: TextStyle(
                color: selectedMenu == 0 ? AppColor.appColor : Colors.grey,
              ),
            )
          ],
        ),
      ),
      Container(
        color: selectedMenu == 1 ? Color(0xffECE4FE) : Colors.transparent,
        height: 45,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: 20,
            ),
            Image.asset(
              selectedMenu == 1
                  ? 'assets/images/ic_setting_selected.png'
                  : 'assets/images/ic_setting_unselected.png',
              height: 20,
              width: 20,
            ),
            SizedBox(
              width: 20,
            ),
            Text(
              'Settings',
              style: TextStyle(
                color: selectedMenu == 1 ? AppColor.appColor : Colors.grey,
              ),
            )
          ],
        ),
      ),
      Container(
        color: selectedMenu == 2 ? Color(0xffECE4FE) : Colors.transparent,
        height: 45,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: 20,
            ),
            Image.asset(
              selectedMenu == 2
                  ? 'assets/images/ic_seller_tool_selected.png'
                  : 'assets/images/ic_seller_tool_unselected.png',
              height: 20,
              width: 20,
            ),
            SizedBox(
              width: 20,
            ),
            Text(
              'Seller Tool',
              style: TextStyle(
                color: selectedMenu == 2 ? AppColor.appColor : Colors.grey,
              ),
            )
          ],
        ),
      ),
      Container(
        color: selectedMenu == 3 ? Color(0xffECE4FE) : Colors.transparent,
        height: 45,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: 20,
            ),
            Image.asset(
              selectedMenu == 3
                  ? 'assets/images/ic_favorite_selected.png'
                  : 'assets/images/ic_favorite_unselected.png',
              height: 20,
              width: 20,
            ),
            SizedBox(
              width: 20,
            ),
            Text(
              'Favourite',
              style: TextStyle(
                color: selectedMenu == 3 ? AppColor.appColor : Colors.grey,
              ),
            )
          ],
        ),
      ),
      Container(
        color: selectedMenu == 4 ? Color(0xffECE4FE) : Colors.transparent,
        height: 45,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: 20,
            ),
            Image.asset(
              selectedMenu == 4
                  ? 'assets/images/ic_follow_selected.png'
                  : 'assets/images/ic_follow_unselected.png',
              height: 20,
              width: 20,
            ),
            SizedBox(
              width: 20,
            ),
            Text(
              'Follow',
              style: TextStyle(
                color: selectedMenu == 4 ? AppColor.appColor : Colors.grey,
              ),
            )
          ],
        ),
      ),
      Container(
        color: selectedMenu == 5 ? Color(0xffECE4FE) : Colors.transparent,
        height: 45,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: 20,
            ),
            Image.asset(
              selectedMenu == 5
                  ? 'assets/images/ic_about_us_selected.png'
                  : 'assets/images/ic_about_us_unselected.png',
              height: 20,
              width: 20,
            ),
            SizedBox(
              width: 20,
            ),
            Text(
              'About Us',
              style: TextStyle(
                color: selectedMenu == 5 ? AppColor.appColor : Colors.grey,
              ),
            )
          ],
        ),
      ),
      Container(
        color: selectedMenu == 6 ? Color(0xffECE4FE) : Colors.transparent,
        height: 45,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: 20,
            ),
            Image.asset(
              selectedMenu == 6
                  ? 'assets/images/ic_contact_us_selected.png'
                  : 'assets/images/ic_contact_us_unselected.png',
              height: 20,
              width: 20,
            ),
            SizedBox(
              width: 20,
            ),
            Text(
              'Contact Us',
              style: TextStyle(
                color: selectedMenu == 6 ? AppColor.appColor : Colors.grey,
              ),
            )
          ],
        ),
      ),
      Container(
        color: selectedMenu == 7 ? Color(0xffECE4FE) : Colors.transparent,
        height: 45,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: 20,
            ),
            Image.asset(
              selectedMenu == 7
                  ? 'assets/images/ic_help_selected.png'
                  : 'assets/images/ic_help_unselected.png',
              height: 20,
              width: 20,
            ),
            SizedBox(
              width: 20,
            ),
            Text(
              'Help',
              style: TextStyle(
                color: selectedMenu == 7 ? AppColor.appColor : Colors.grey,
              ),
            )
          ],
        ),
      ),
    ];
    return Stack(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width / 1.6,
          child: Drawer(
              child: Column(
            children: <Widget>[
              Container(
                  padding: EdgeInsets.all(12),

                  // height: 110,
                  // color:Color(0xffF84546),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 25,
                      ),
                      Container(
                        alignment: Alignment.centerRight,
                        height: 87,
                        width: 87,
                        decoration: BoxDecoration(
                            color: AppColor.appColor, shape: BoxShape.circle),
                        child: GestureDetector(
                          child: Container(
                              // alignment: Alignment.centerRight,
                              height: 80,
                              width: 80,
                              decoration: BoxDecoration(
                                  color: Colors.white, shape: BoxShape.circle),
                              child: ClipOval(
                                  child: Image.asset('assets/images/dp2.jpg',
                                      fit: BoxFit.cover))),
                          onTap: () {},
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                          child: Text(
                        'John Doe',
                        style: TextStyle(
                            color: AppColor.appColor,
                            fontSize: 16,
                            fontWeight: FontWeight.w400),
                      )),
                      SizedBox(
                        height: 12,
                      ),
                      Container(
                          child: Text(
                        'Johdoe@gmail.com',
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 12.5,
                            fontWeight: FontWeight.w300),
                      )),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          MaterialButton(
                            onPressed: () {},
                            height: 25,
                            minWidth: 75,
                            color: AppColor.appColor,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 2),
                              child: Text(
                                Strings.buyer,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          MaterialButton(
                            onPressed: () {},
                            height: 25,
                            minWidth: 75,
                            color: Color(0xffECE4FE),
                            child: Padding(
                              padding: const EdgeInsets.only(top: 2),
                              child: Text(
                                Strings.seller,
                                style: TextStyle(
                                    color: AppColor.appColor,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ),
                        ],
                      ),
                      // SizedBox(height: 15,),
                    ],
                  )),
              Expanded(
                  child: MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child: Container(
                  margin: EdgeInsets.all(8),
                  child: ListView.builder(
                      itemCount: listOFcell.length,
                      itemBuilder: (context, int index) {
                        return GestureDetector(
                            onTap: () {
                              selectedMenu = index;
                              setState(() {});
                              if (index == 0) {
                                Navigator.pop(context);
                                // Navigator.push(
                                //                       context,
                                //                       NoAnimationMaterialPageRoute(
                                //                           builder: (context) => ProfilePage()));
                              } else if (index == 1) {
                                Navigator.pop(context);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => SettingPage()));
                              } else if (index == 2) {
                                Navigator.pop(context);
                                // Navigator.push(
                                //                       context,
                                //                       NoAnimationMaterialPageRoute(
                                //                           builder: (context) => ProfilePage()));
                              } else if (index == 3) {
                                Navigator.pop(context);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => FavouritePage()));
                              } else if (index == 4) {
                                Navigator.pop(context);
                              } else if (index == 5) {
                                Navigator.pop(context);
                              } else if (index == 6) {
                                Navigator.pop(context);
                              } else if (index == 7) {
                                Navigator.pop(context);
                              }
                            },
                            child: Container(
                                color: Colors.transparent,
                                child: Column(
                                  children: [
                                    listOFcell[index],
                                    SizedBox(
                                      height: 10,
                                    )
                                  ],
                                )));
                      }),
                ),
              )),
              GestureDetector(
                child: Container(
                  color: AppColor.appColor,
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Logout',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w600),
                      )
                    ],
                  ),
                ),
                onTap: () {
                  preferences.remove(PrefKeys.IS_LOGIN);
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (context) => SignInScreen()),
                      (Route<dynamic> route) => false);
                },
              ),
            ],
          )),
        ),
        // Center(
        //   child: StreamBuilder<bool>(
        //     initialData: false,
        //     stream: blocdata.loadingSTREAM,
        //     builder: (BuildContext context, snapshot) {
        //       return snapshot.data == true
        //           ? loding(context)
        //           : Container(
        //               width: 0,
        //               height: 0,
        //             );
        //     },
        //   ),
        // ),
      ],
    );
  }
}
