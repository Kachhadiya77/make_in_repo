import 'package:flutter/material.dart';
import 'package:makeinindiatrade/src/Images/images.dart';
import 'package:makeinindiatrade/src/bloc/citybloc.dart';
import 'package:makeinindiatrade/src/bloc/password_visibility_bloc.dart';
import 'package:makeinindiatrade/src/constant/constant.dart';
import 'package:makeinindiatrade/src/constant/strings.dart';
import 'package:provider/provider.dart';

class SelectYourCity extends StatelessWidget {
  final TextEditingController _selectCitytxt = TextEditingController();

  final List<dynamic> cistyList = [
    {'id': 1, 'state_name': 'Andaman and Nicobar Islands'},
    {'id': 2, 'state_name': 'Andhra Pradesh'},
    {'id': 3, 'state_name': 'Arunachal Pradesh'},
    {'id': 4, 'state_name': 'Assam'},
    {'id': 5, 'state_name': 'Bihar'},
    {'id': 6, 'state_name': 'Chandigarh'},
    {'id': 7, 'state_name': 'Chhattisgarh'},
    {'id': 8, 'state_name': 'Dadra and Nagar Haveli'},
    {'id': 9, 'state_name': 'Daman and Diu'},
    {'id': 10, 'state_name': 'Delhi'},
    {'id': 11, 'state_name': 'Goa'},
    {'id': 12, 'state_name': 'Gujarat'},
    {'id': 13, 'state_name': 'Haryana'},
    {'id': 14, 'state_name': 'Himachal Pradesh'},
    {'id': 15, 'state_name': 'Jammu and Kashmir'},
    {'id': 16, 'state_name': 'Jharkhand'},
    {'id': 17, 'state_name': 'Karnataka'},
    {'id': 18, 'state_name': 'Kenmore'},
    {'id': 19, 'state_name': 'Kerala'},
    {'id': 20, 'state_name': 'Lakshadweep'},
    {'id': 21, 'state_name': 'Madhya Pradesh'},
    {'id': 22, 'state_name': 'Maharashtra'},
    {'id': 23, 'state_name': 'Manipur'},
    {'id': 24, 'state_name': 'Meghalaya'},
    {'id': 25, 'state_name': 'Mizoram'},
    {'id': 26, 'state_name': 'Nagaland'},
    {'id': 27, 'state_name': 'Narora'},
    {'id': 28, 'state_name': 'Natwar'},
    {'id': 29, 'state_name': 'Odisha'},
    {'id': 30, 'state_name': 'Paschim Medinipur'},
    {'id': 31, 'state_name': 'Puducherry'},
    {'id': 32, 'state_name': 'Punjab'},
    {'id': 33, 'state_name': 'Rajasthan'},
    {'id': 34, 'state_name': 'Sikkim'},
    {'id': 35, 'state_name': 'Tamil Nadu'},
    {'id': 36, 'state_name': 'Telangana'},
    {'id': 37, 'state_name': 'Tripura'},
    {'id': 38, 'state_name': 'Uttar Pradesh'},
    {'id': 39, 'state_name': 'Uttarakhand'},
    {'id': 40, 'state_name': 'Vaishali'},
    {'id': 41, 'state_name': 'West Bengal'},
    {'id': 4121, 'state_name': 'Pujab'}
  ];

  @override
  Widget build(BuildContext context) {
    final cityBlocController = Provider.of<CityListBloc>(context);
      final visibalConfPassword = Provider.of<VisibleConfPassBloc>(context);
    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(50),
          child: Column(
            children: [
              _selectyourcity(),
              _setImage(),
              _dummyContent(),
              SizedBox(
                height: 30,
              ),
              _yourselectLocation(),
              SizedBox(
                height: 20,
              ),
              _selectedCity(),
              SizedBox(
                height: 20,
              ),
              _allCitiesField(cityBlocController),
              SizedBox(
                height: 35,
              ),
              listOfcity(cityBlocController),
              exploreMoreCity(cityBlocController),
            ],
          ),
        ),
      )),
    );
  }

  Widget _selectyourcity() {
    return Center(
        child: Text(
      Strings.selectyourlocation,
      style: TextStyle(
          fontSize: 16.5, fontWeight: FontWeight.w600, color: Colors.black),
      textAlign: TextAlign.center,
    ));
  }

  Widget _setImage() {
    return Image.asset(
      Images.CITYIMAGE,
      alignment: Alignment.center,
    );
  }

  Widget _dummyContent() {
    return Center(
        child: Text(
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus auctor nulla in sapien sollicitudin,',
      style: TextStyle(
          fontSize: 13, fontWeight: FontWeight.w200, color: Colors.grey),
      textAlign: TextAlign.center,
    ));
  }

  Widget _yourselectLocation() {
    return Align(
        alignment: Alignment.topLeft,
        child: Text(
          Strings.yourselectedLocationis,
          style: TextStyle(
              fontSize: 13.0, fontWeight: FontWeight.w300, color: Colors.black),
          textAlign: TextAlign.left,
        ));
  }

  Widget _selectedCity() {
    return Align(
        alignment: Alignment.topLeft,
        child: Text(
          'Pune',
          style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.w400,
              color: Color(0xff6621FA)),
          textAlign: TextAlign.left,
        ));
  }

  Widget _allCitiesField(CityListBloc cityBlocController) {
    return SingleLineTextField(
      hintText: 'All Cities',
      controller: _selectCitytxt,
      onChange: (txt) {
        // txtString = txt;
        cityBlocController.citySINK(cistyList, txt);
        cityBlocController.stringSINK(txt);
      },
      contentPadding: EdgeInsets.only(top: 15),
      suffixIcon: IconButton(
          icon: Icon(
            Icons.search,
            color: AppColor.appColor,
            size: 20,
          ),
          onPressed: () {}),
    );
  }

// List datasearch = [];
  Widget listOfcity(CityListBloc cityBlocController) {
    return StreamBuilder<bool>(
        stream: cityBlocController.expandedSTREAM,
        initialData: false,
        builder: (context, snapshotTF) {
          return StreamBuilder<List>(
              stream: cityBlocController.citySTREAM,
              builder: (context, AsyncSnapshot<List> snapshot) {
                if (!snapshot.hasData) {
                  cityBlocController.citySINK(cistyList, '');
                  return CircularProgressIndicator();
                }

                // datasearch = [];
                // if(txtString.isNotEmpty)
                // {
                //     for(var item in snapshot.data)
                //     {
                //         if(txtString.toLowerCase().contains(item['state_name'].toString()))
                //         {
                //             datasearch.add(item);
                //         }
                //     }
                // }
                // else
                // {
                //   datasearch = snapshot.data;
                // }

                return snapshot.data.isEmpty
                    ? SizedBox()
                    : ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: snapshotTF.data
                            ? snapshot.data.length
                            : snapshot.data.length > 5
                                ? 5
                                : snapshot.data.length,
                        itemBuilder: (context, int index) {
                          return Container(
                            height: 35,
                            child: Text(
                              snapshot.data[index]['state_name'],
                              style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black),
                            ),
                          );
                        });
              });
        });
  }

  Widget exploreMoreCity(CityListBloc cityBlocController) {
    return Align(
        alignment: Alignment.topLeft,
        child: StreamBuilder<bool>(
            initialData: false,
            stream: cityBlocController.expandedSTREAM,
            builder: (context, snapshot) {
              return GestureDetector(
                  onTap: () {
                    if (snapshot.data) {
                      cityBlocController.expandedSINK(false);
                    } else {
                      cityBlocController.expandedSINK(true);
                    }
                  },
                  child: Text(
                    Strings.exploreMoreCity,
                    style: TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff6621FA)),
                    textAlign: TextAlign.left,
                  ));
            }));
  }
}
