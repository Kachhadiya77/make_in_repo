import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:makeinindiatrade/src/bloc/favouriteBloc.dart';

import 'package:makeinindiatrade/src/constant/constant.dart';
import 'package:makeinindiatrade/src/constant/strings.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class FavouritePage extends StatelessWidget {
  
  // const FavouritePage({Key key}) : super(key: key);
  final  _controller = ScrollController();
    final List dummyUrls = [
      'https://images.pexels.com/photos/258154/pexels-photo-258154.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
      'https://images.pexels.com/photos/271639/pexels-photo-271639.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      'https://images.pexels.com/photos/164595/pexels-photo-164595.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      'https://images.pexels.com/photos/573552/pexels-photo-573552.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      'https://images.pexels.com/photos/271624/pexels-photo-271624.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
    ];
  @override
  Widget build(BuildContext context) {

        final favBlocController = Provider.of<FavouriteBloc>(context);
    return Scaffold(
       appBar: AppBar(
        leading: GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: Image.asset('assets/images/ic_back_white.png',height: 18,width: 18,)),
          backgroundColor: AppColor.appColor,
          centerTitle: true,
          title: Text(Strings.favourite,style: TextStyle(color: Colors.white,fontWeight: FontWeight.w400,fontSize: 16.5),),
          
      ),
      body:  Column(
        children: [
          Expanded(child: buildGridView(favBlocController)),
        ],
      ),
    );
  }


     Widget buildGridView(FavouriteBloc favBlocController) {
    return StreamBuilder<List>(
        stream: favBlocController.favouriteSTREAM,
        builder: (context,AsyncSnapshot snapshot) {
    if(!snapshot.hasData){
      favBlocController.favouriteSINK(dummyUrls);
            return Center(child: CircularProgressIndicator());
      
    }

    return snapshot.data.isEmpty ? SizedBox() :
    
    GridView.builder(
      controller: _controller, 
      padding: EdgeInsets.only(left: 10, right: 10, top: 0),
      // physics: NeverScrollableScrollPhysics(),
      // shrinkWrap: true,
      itemCount: snapshot.data.length,
      gridDelegate:
           SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2,childAspectRatio: 0.81,),
      itemBuilder: (context,int index) {
        return  GestureDetector(
              onTap: (){
               
              },
                          child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
          elevation: 0.6,
          
          child:
          Column(
            children: [
              Expanded(
                              child: Container(
                  decoration: BoxDecoration(
                       borderRadius:  BorderRadius.all(Radius.circular(8)),
                        // color: Colors.transparent,
                    ),
                    
                    child: 
                    ClipRRect(
                      borderRadius:  BorderRadius.only(topLeft: Radius.circular(8),topRight: Radius.circular(8)),

                      child:snapshot.data != [] && snapshot.data.isNotEmpty ? CachedNetworkImage(
                        
                                                                    imageUrl: snapshot.data[index].toString(),
                                                                    fit: BoxFit.cover,
                                                                    
                                                                    placeholder: (context, url) => Shimmer.fromColors(
                                                                        baseColor: Colors.grey[200],
                                                                        highlightColor: Colors.grey[300],
                                                                        child: Container(
                                                                          decoration: BoxDecoration(
                                                                             borderRadius:  BorderRadius.all(Radius.circular(8)),
                                                                          ),
                                                                      height: MediaQuery.of(context).size.height / 3.5,
                                                                      width: MediaQuery.of(context).size.width -30,
                                                                          // color: Colors.grey[200],
                                                                        )),
                                                                      height: MediaQuery.of(context).size.height / 3.5,
                                                                      width: MediaQuery.of(context).size.width -30,
                                                                      
                                                                    errorWidget: (context, url, error) =>
                                                                        Icon(Icons.error),
                                                                  ) : Shimmer(child: Container(
                                                                      decoration: BoxDecoration(
                                                                         borderRadius:  BorderRadius.all(Radius.circular(8)),
                                                                         color: Colors.grey[200],
                                                                      ),
                                                                  height: MediaQuery.of(context).size.height / 3.5,
                                                                  width: MediaQuery.of(context).size.width -30,
                                                                      // color: Colors.grey[200],
                                                                    ), gradient: null),
                    ),
                    // Image.network(snapshot.data.documents[index]['productimage'][0],fit: BoxFit.contain,),
                ),
              ),
              Container(
                height: 35,
                decoration: BoxDecoration(
                        borderRadius:  BorderRadius.only(bottomLeft: Radius.circular(8),bottomRight: Radius.circular(8)),
                        // color: Colors.green[200],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                      Container(
                        padding: EdgeInsets.all(5),
                        child: Text('Automatic jigger machine',overflow: TextOverflow.ellipsis,maxLines: 1,style: TextStyle(fontSize: 10,color: AppColor.appColor),)),
                        Image.asset('assets/images/ic_favorite_red.png',height: 20,width: 20,)
                ],),
              )
            ],
          ),

        ),
            );
        
        
      },
    );
        }
      );
  }
}

