import 'package:flutter/material.dart';
import 'package:makeinindiatrade/src/constant/constant.dart';
import 'package:makeinindiatrade/src/constant/strings.dart';

class ForgotPasswordScreen extends StatelessWidget {
  final TextEditingController _emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            _appLogo(),
            _email(context),
            _signUpButton(context),
            _signInText(context),
          ],
        ),
      ),
    );
  }

  Widget _appLogo() {
    return Padding(
      padding: const EdgeInsets.only(top: 110.0),
      child: AppLogo(),
    );
  }

  Widget _email(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 70.0),
      child: CustomTextField(
        controller: _emailController,
        hintText: Strings.emailID,
        labelText: Strings.emailID,
      ),
    );
  }

  Widget _signUpButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 60.0),
      child: CustomButton(
        width: 200,
        title: Strings.getPassword.toUpperCase(),
        onPressed: () {
          FocusScope.of(context).unfocus();
        },
      ),
    );
  }

  Widget _signInText(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 40.0, bottom: 40.0),
      child: InkWell(
        onTap: () => _navigateToSignIn(context),
        child: Text(
          Strings.back.toUpperCase(),
          style: TextStyle(
            color: AppColor.appColor,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }

  // All Navigation related to this page is mentioned below.
  void _navigateToSignIn(BuildContext context) {
    Navigator.pop(context);
  }
}
