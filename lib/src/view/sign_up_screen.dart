import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:makeinindiatrade/src/Images/images.dart';
import 'package:makeinindiatrade/src/bloc/auto_validate_bloc.dart';
import 'package:makeinindiatrade/src/bloc/password_visibility_bloc.dart';
import 'package:makeinindiatrade/src/constant/constant.dart';
import 'package:makeinindiatrade/src/constant/strings.dart';
import 'package:makeinindiatrade/src/view/home.dart';
import 'package:makeinindiatrade/src/reusable/app_utilis.dart';
import 'package:makeinindiatrade/src/view/select_city.dart';
import 'package:provider/provider.dart';

class SignUpScreen extends StatelessWidget {
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _cityController = TextEditingController();
  final TextEditingController _pincodeController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPassController = TextEditingController();

  final FocusNode _firstNameFocus = FocusNode();
  final FocusNode _lastNameFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _phoneNumberFocus = FocusNode();
  final FocusNode _cityFocus = FocusNode();
  final FocusNode _pincodeFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _confirmPassFocus = FocusNode();

  // bool _autoValidate = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final visibalPassword = Provider.of<VisiblePassBloc>(context);
    final visibalConfPassword = Provider.of<VisibleConfPassBloc>(context);
    final autoValidate = Provider.of<AutoValidate>(context);

    return Scaffold(
      body: SingleChildScrollView(
        child: StreamBuilder<bool>(
            stream: autoValidate.autoValidateStream,
            initialData: false,
            builder: (context, validate) {
              return Form(
                key: _formKey,
                autovalidate: validate.data,
                child: Column(
                  children: [
                    _appLogo(),
                    _firstName(context),
                    _lastName(context),
                    _email(context),
                    _phoneNumber(context),
                    _cityDropDown(context,visibalConfPassword),
                    _pincode(context),
                    _password(context, visibalPassword),
                    _confirmPassword(context, visibalConfPassword),
                    _signUpButton(context, autoValidate),
                    _signInText(context),
                  ],
                ),
              );
            }),
      ),
    );
  }

  Widget _appLogo() {
    return Padding(
      padding: const EdgeInsets.only(top: 50.0),
      child: AppLogo(),
    );
  }

  Widget _firstName(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 40.0),
      child: CustomTextField(
        controller: _firstNameController,
        focusNode: _firstNameFocus,
        textInputAction: TextInputAction.next,
        onSubmitted: (_) => FocusScope.of(context).requestFocus(_lastNameFocus),
        hintText: Strings.firstname,
        labelText: Strings.firstname,
        validator: _firstNameValidator,
        onSave: (value) {
          _firstNameController.text = value;
        },
      ),
    );
  }

  String _firstNameValidator(String value) {
    if (_firstNameController.text.length > 1) {
      return null;
    } else {
      return Strings.validateName;
    }
  }

  Widget _lastName(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30.0),
      child: CustomTextField(
        controller: _lastNameController,
        focusNode: _lastNameFocus,
        textInputAction: TextInputAction.next,
        onSubmitted: (_) => FocusScope.of(context).requestFocus(_emailFocus),
        hintText: Strings.lastname,
        labelText: Strings.lastname,
        validator: _lastNameValidator,
        onSave: (String val) {
          _lastNameController.text = val;
        },
      ),
    );
  }

  String _lastNameValidator(String value) {
    if (_lastNameController.text.length > 1) {
      return null;
    } else {
      return Strings.validateName;
    }
  }

  Widget _email(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30.0),
      child: CustomTextField(
        controller: _emailController,
        focusNode: _emailFocus,
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.emailAddress,
        onSubmitted: (_) =>
            FocusScope.of(context).requestFocus(_phoneNumberFocus),
        hintText: Strings.emailID,
        labelText: Strings.emailID,
        validator: _emailValidator,
        onSave: (String val) {
          _emailController.text = val;
        },
      ),
    );
  }

  String _emailValidator(String value) {
    const Pattern pattern = MRegex.EMAIL_REGEX;
    final RegExp regex = RegExp(pattern);
    if (regex.hasMatch(value)) {
      return null;
    } else {
      return Strings.validateName;
    }
  }

  Widget _phoneNumber(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30.0),
      child: CustomTextField(
        controller: _phoneNumberController,
        focusNode: _phoneNumberFocus,
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.number,
        onSubmitted: (_) => FocusScope.of(context).requestFocus(_pincodeFocus),
        hintText: Strings.phoneNumber,
        labelText: Strings.phoneNumber,
        validator: _phoneValidator,
        onSave: (String val) {
          _phoneNumberController.text = val;
        },
      ),
    );
  }

String _phoneValidator(String value) {
    if (_phoneNumberController.text.length == 10) {
      return null;
    } else {
      return Strings.validatePhone;
    }
  }
  Widget _cityDropDown(BuildContext context,VisibleConfPassBloc visibalConfPassword) {
    return Padding(
      padding: const EdgeInsets.only(top: 30.0),
      child: StreamBuilder<String>(
        stream: visibalConfPassword.txtSTREAM,
        builder: (context,AsyncSnapshot snapshot) {
          _cityController.text = snapshot.data;
          return CustomTextField(
            onTap: (){
                              Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => SelectYourCity(),
          ),
    );
            },
            onChanged: (txt){
              visibalConfPassword.txtSINK(txt);
            },
              controller: _cityController,
              focusNode: _cityFocus,
              readOnly: true,
              isSuffix: true,
              suffixIcon: InkWell(
                onTap: () {
          
                },
                
                child: Icon(
          Icons.arrow_drop_down,
          size: 35.0,
                ),
              ),
              textInputAction: TextInputAction.next,
              onSubmitted: (_) => FocusScope.of(context).requestFocus(_pincodeFocus),
              hintText: Strings.city,
              labelText: Strings.city,
            );
        }
      ),
    );
  }

  Widget _pincode(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30.0),
      child: CustomTextField(
        controller: _pincodeController,
        focusNode: _pincodeFocus,
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.next,
        onSubmitted: (_) => FocusScope.of(context).requestFocus(_passwordFocus),
        hintText: Strings.pincode,
        labelText: Strings.pincode,
        validator: _pincodeValidator,
        onSave: (String val) {
          _pincodeController.text = val;
        },
      ),
    );
  }

  String _pincodeValidator(String value) {
    if (_pincodeController.text.length >= 3) {
      return null;
    } else {
      return Strings.validatePincode;
    }
  }

  Widget _password(
    BuildContext context,
    VisiblePassBloc visibalPassword,
  ) {
    return Padding(
      padding: const EdgeInsets.only(top: 30.0),
      child: StreamBuilder<bool>(
          stream: visibalPassword.visibilityStream,
          initialData: false,
          builder: (context, visibility) {
            return CustomTextField(
              controller: _passwordController,
              focusNode: _passwordFocus,
              textInputAction: TextInputAction.next,
              onSubmitted: (_) =>
                  FocusScope.of(context).requestFocus(_confirmPassFocus),
              hintText: Strings.password,
              labelText: Strings.password,
              obscureText: !visibility.data ? true : false,
              isSuffix: true,
              suffixIcon: _showPassword(visibalPassword, visibility.data),
              validator: _passwordValidator,
              onSave: (String val) {
                _passwordController.text = val;
              },
            );
          }),
    );
  }

  String _passwordValidator(String value) {
    if (_passwordController.text.isNotEmpty) {
      return null;
    } else {
      return Strings.validatePassword;
    }
  }

  Widget _showPassword(dynamic visibalPassword, bool visibility) {
    return InkWell(
      onTap: () {
        if (visibility) {
          visibalPassword.visibilitySink(false);
        } else {
          visibalPassword.visibilitySink(true);
        }
      },
      child: Container(
        height: 20,
        width: 20,
        margin: EdgeInsets.all(12.0),
        child: visibility
            ? Image.asset(
                Images.VISIBLE,
                color: AppColor.appColor,
              )
            : Image.asset(
                Images.INVISIBLE,
                color: Colors.grey[500],
              ),
      ),
    );
  }

  Widget _confirmPassword(
      BuildContext context, VisibleConfPassBloc visibalBloc) {
    return Padding(
      padding: const EdgeInsets.only(top: 30.0),
      child: StreamBuilder<bool>(
          stream: visibalBloc.visibilityStream,
          initialData: false,
          builder: (context, visibility) {
            return CustomTextField(
              controller: _confirmPassController,
              focusNode: _confirmPassFocus,
              textInputAction: TextInputAction.done,
              onSubmitted: (_) => FocusScope.of(context).unfocus(),
              hintText: Strings.confirmPassword,
              labelText: Strings.confirmPassword,
              obscureText: !visibility.data ? true : false,
              isSuffix: true,
              suffixIcon: _showPassword(visibalBloc, visibility.data),
              validator: _confirmPasswordValidator,
              onSave: (String val) {
                _confirmPassController.text = val;
              },
            );
          }),
    );
  }

  String _confirmPasswordValidator(String value) {
    if (_passwordController.text == _confirmPassController.text) {
      return null;
    } else {
      return Strings.confirmPassword;
    }
  }

  void _validateInputs(AutoValidate autoValidate) {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
    } else {
      autoValidate.autoValidateSink(true);
    }
  }

  Widget _signUpButton(BuildContext context, AutoValidate autoValidate) {
    return Padding(
      padding: const EdgeInsets.only(top: 60.0),
      child: CustomButton(
        title: Strings.signIn,
        onPressed: () {
          FocusScope.of(context).unfocus();
                               Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => HomePage(),
          ),
    );

        },
      ),
    );
  }

  Widget _signInText(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 40.0, bottom: 40.0),
      child: RichText(
        text: TextSpan(
          style: TextStyle(fontWeight: FontWeight.w600),
          children: <TextSpan>[
            TextSpan(
              text: Strings.existingUser,
              style: TextStyle(color: Colors.black),
            ),
            TextSpan(
              text: ' ${Strings.signIn}',
              style: TextStyle(color: AppColor.appColor),
              recognizer: TapGestureRecognizer()
                ..onTap = () => _navigateToSignIn(context),
            ),
          ],
        ),
      ),
    );
  }

  void _navigateToSignIn(BuildContext context) {
    Navigator.pop(context);
  }
}
