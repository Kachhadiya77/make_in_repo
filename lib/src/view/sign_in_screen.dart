import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:makeinindiatrade/src/Images/images.dart';
import 'package:makeinindiatrade/src/bloc/login_bloc.dart';
import 'package:makeinindiatrade/src/bloc/password_visibility_bloc.dart';
import 'package:makeinindiatrade/src/bloc/validation.dart';
import 'package:makeinindiatrade/src/constant/constant.dart';
import 'package:makeinindiatrade/src/constant/strings.dart';
import 'package:makeinindiatrade/src/model/login_model.dart';
import 'package:makeinindiatrade/src/shared_preference/prefs_keys.dart';
import 'package:makeinindiatrade/src/view/forgot_password_screen.dart';
import 'package:makeinindiatrade/src/view/home.dart';
import 'package:makeinindiatrade/src/view/sign_up_screen.dart';
import 'package:provider/provider.dart';

class SignInScreen extends StatelessWidget {
  // TextEditing Controller
  final TextEditingController _numberController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  // Focus Node
  final FocusNode _numberFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();

  @override
  Widget build(BuildContext context) {
    final visibilityBloc = Provider.of<PasswordVisibilityBloc>(context);
    final validationBloc = Provider.of<ValidationBloc>(context);
    final loginBloc = Provider.of<LoginBloc>(context);
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              _appLogo(),
              _emailAndPassswordBox(
                context,
                visibilityBloc,
                validationBloc,
                loginBloc,
              ),
              _signUpText(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _appLogo() => AppLogo(size: 200);

  Widget _emailAndPassswordBox(
      BuildContext context,
      PasswordVisibilityBloc visibilityBloc,
      ValidationBloc validationBloc,
      LoginBloc loginBloc) {
    return Column(
      children: [
        _enterNumber(context, validationBloc),
        _enterPassword(visibilityBloc, validationBloc),
        _forgetPassword(context),
        _signInButton(context, validationBloc, loginBloc),
      ],
    );
  }

  Widget _enterNumber(BuildContext context, ValidationBloc validationBloc) {
    return StreamBuilder(
        stream: validationBloc.number,
        builder: (context, snapshot) {
          return CustomTextField(
            controller: _numberController,
            focusNode: _numberFocus,
            keyboardType: TextInputType.number,
            onChanged: validationBloc.changeNumber,
            errorText: snapshot.error,
            textInputAction: TextInputAction.next,
            onSubmitted: (_) =>
                FocusScope.of(context).requestFocus(_passwordFocus),
            hintText: Strings.phoneNumber,
            labelText: Strings.phoneNumber,
          );
        });
  }

  Widget _enterPassword(
      PasswordVisibilityBloc visibilityBloc, ValidationBloc validationBloc) {
    return StreamBuilder<bool>(
        stream: visibilityBloc.visibilityStream,
        initialData: false,
        builder: (context, visibility) {
          return Padding(
            padding: const EdgeInsets.only(top: 40.0),
            child: StreamBuilder(
                stream: validationBloc.password,
                builder: (context, snapshot) {
                  return CustomTextField(
                    controller: _passwordController,
                    focusNode: _passwordFocus,
                    onChanged: validationBloc.changePassword,
                    errorText: snapshot.error,
                    hintText: Strings.password,
                    labelText: Strings.password,
                    obscureText: !visibility.data ? true : false,
                    isSuffix: true,
                    suffixIcon: _showPassword(visibilityBloc, visibility.data),
                  );
                }),
          );
        });
  }

  Widget _showPassword(PasswordVisibilityBloc visibilityBloc, bool visibility) {
    return InkWell(
      onTap: () {
        if (visibility) {
          visibilityBloc.visibilitySink(false);
        } else {
          visibilityBloc.visibilitySink(true);
        }
      },
      child: Container(
        height: 20,
        width: 20,
        margin: EdgeInsets.all(12.0),
        child: visibility
            ? Image.asset(
                Images.VISIBLE,
                color: AppColor.appColor,
              )
            : Image.asset(
                Images.INVISIBLE,
                color: Colors.grey[500],
              ),
      ),
    );
  }

  Widget _forgetPassword(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 15.0, right: 30.0),
      child: Align(
        alignment: Alignment.centerRight,
        child: InkWell(
          onTap: () => _navigateToForgotPassword(context),
          child: Text(
            Strings.forgot.toUpperCase(),
            style: TextStyle(
              color: AppColor.appColor,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      ),
    );
  }

  Widget _signInButton(BuildContext context, ValidationBloc validationBloc,
      LoginBloc loginBloc) {
    return Padding(
      padding: const EdgeInsets.only(top: 50.0),
      child: StreamBuilder(
          stream: validationBloc.submitValid,
          builder: (context, snapshot) {
            return StreamBuilder<LoginModel>(
                stream: loginBloc.loginStream,
                builder: (context, loginData) {
                  return CustomButton(
                    title: Strings.signIn,
                    onPressed: snapshot.hasData
                        ? () => _pressLoginButton(loginBloc, context)
                        : null,
                  );
                });
          }),
    );
  }

  void _pressLoginButton(LoginBloc loginBloc, BuildContext context) {
    Indicators().indicatorPopupWillNotPop(context);
    loginBloc
        .loginSink(_numberController.text, _passwordController.text)
        .then((value) {
      print(value.data);
      preferences.setBool(PrefKeys.IS_LOGIN, true);
      Indicators().hideIndicator(context);
      _navigateToHomePage(context);
      
    });
  }

  Widget _signUpText(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
      child: RichText(
        text: TextSpan(
          style: TextStyle(fontWeight: FontWeight.w600),
          children: <TextSpan>[
            TextSpan(
              text: Strings.newUser,
              style: TextStyle(color: Colors.black),
            ),
            TextSpan(
              text: ' ${Strings.signUp}',
              style: TextStyle(color: AppColor.appColor),
              recognizer: TapGestureRecognizer()
                ..onTap = () => _navigateToSignUp(context),
            ),
          ],
        ),
      ),
    );
  }

  // All Navigation related to this page is mentioned below.
  void _navigateToSignUp(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SignUpScreen(),
      ),
    );
  }

  void _navigateToForgotPassword(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ForgotPasswordScreen(),
      ),
    );
  }

  void _navigateToHomePage(BuildContext context) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => HomePage(),
      ),
    );
  }
}
