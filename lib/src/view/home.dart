import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:makeinindiatrade/src/Images/images.dart';
import 'package:makeinindiatrade/src/bloc/category_bloc.dart';
import 'package:makeinindiatrade/src/constant/constant.dart';
import 'package:makeinindiatrade/src/constant/strings.dart';
import 'package:makeinindiatrade/src/model/category_model.dart';
import 'package:makeinindiatrade/src/view/draweBar.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  // HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final GlobalKey<ScaffoldState> _feedsScaffoldsKey =
      GlobalKey<ScaffoldState>();
  List<dynamic> gridlist = [
    Image.asset(
      Images.CITYIMAGE,
    ),
    Image.asset(
      Images.INVISIBLE,
    ),
    Image.asset(
      Images.LOGO,
    ),
    Image.asset(
      Images.ic_architecture_materials_services,
    ),
    Image.asset(
      Images.agriculture,
    ),
    Image.asset(
      Images.ic_bags_cases,
    ),
    Image.asset(
      Images.ic_bags_cases,
    ),
    Image.asset(
      Images.ic_bags_cases,
    ),
    Image.asset(
      Images.agriculture,
    ),
    Image.asset(
      Images.INVISIBLE,
    ),
    Image.asset(
      Images.LOGO,
    ),
    Image.asset(
      Images.ic_architecture_materials_services,
    ),
    Image.asset(
      Images.agriculture,
    ),
    Image.asset(
      Images.ic_bags_cases,
    ),
    Image.asset(
      Images.ic_bags_cases,
    ),
    Image.asset(
      Images.ic_bags_cases,
    ),
    Image.asset(
      Images.agriculture,
    ),
  ];

  @override
  void initState() {
    super.initState();
  }

  dd() async {
    await Future.delayed(Duration(seconds: 1));
    final categoryBloc = Provider.of<CategoryBloc>(context);
    categoryBloc.categorySink();
  }

  @override
  Widget build(BuildContext context) {
    final categoryBloc = Provider.of<CategoryBloc>(context);

    return Scaffold(
      drawer: DrawerOnly(),
      key: _feedsScaffoldsKey,
      appBar: AppBar(
        backgroundColor: AppColor.appColor,
        automaticallyImplyLeading: false,
        title: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 5),
              child: Text(
                Strings.makeInIndia,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w300,
                    fontSize: 16),
              ),
            )
          ],
        ),
        leading: GestureDetector(
            onTap: () {
              _feedsScaffoldsKey.currentState.openDrawer();
            },
            child: SizedBox(
                child: Image.asset(
              'assets/images/ic_menu.png',
            ))),
        actions: [
          GestureDetector(
            onTap: () {},
            child: Padding(
              padding: const EdgeInsets.only(right: 15.0),
              child: GestureDetector(
                  onTap: () {},
                  child: Image.asset(
                    Images.SEARCHICON,
                    height: 22,
                    width: 22,
                  )),
            ),
          ),
        ],
        elevation: 0,
        bottom: PreferredSize(
            child: Container(
              padding: EdgeInsets.only(left: 15, right: 15, bottom: 10),
              color: AppColor.appColor,
              height: 35.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Pune',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w400,
                        fontSize: 16.5),
                  ),
                  Image.asset(
                    'assets/images/ic_pin.png',
                    height: 18,
                    width: 18,
                  )
                ],
              ),
            ),
            preferredSize: Size.fromHeight(35.0)),
      ),
      body: Column(
        children: [
          topBanner(),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: StreamBuilder<CategoryModel>(
                  stream: categoryBloc.categoryStream,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Indicators().indicatorWidget();
                    }
                    return GridView.builder(
                      shrinkWrap: true,
                      controller: ScrollController(keepScrollOffset: false),
                      itemCount: snapshot.data.data.data.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,

                        // childAspectRatio: (3 / 4),
                      ),
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          decoration:
                              myBoxDecoration(index, 3, gridlist.length),
                          child: Image.network(
                              snapshot.data.data.data[index].image),
                        );
                      },
                    );
                  }),
            ),
          ),

          // gridSection,
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: ()async  {
          await categoryBloc.categorySink();
        },
        backgroundColor: AppColor.appColor,
        child: Image.asset(
          'assets/images/btn_inquiry.png',
          fit: BoxFit.cover,
          filterQuality: FilterQuality.high,
        ),
      ),
    );
  }

  BoxDecoration myBoxDecoration(
      int index, int gridViewCrossAxisCount, int totallemgth) {
    index++;
    return BoxDecoration(
      border: Border(
        right: BorderSide(
          //                   <--- left side
          color: index % gridViewCrossAxisCount != 0
              ? Colors.black12
              : Colors.transparent,
          width: 1.0,
        ),
        top: BorderSide(
          //                   <--- left side
          color: index >= 4 && index <= 6 ? Colors.black12 : Colors.transparent,
          // index > gridViewCrossAxisCount ? Colors.black12 : Colors.transparent,
          width: 1.0,
        ),
        bottom: BorderSide(
          //                   <--- left side
          color: totallemgth - 1 == index || totallemgth == index
              ? Colors.transparent
              : index > gridViewCrossAxisCount
                  ? Colors.black12
                  : Colors.transparent,
          width: 1.0,
        ),
      ),
    );
  }

  Widget topBanner() {
    final List<String> dummyUrls = [
      'https://images.pexels.com/photos/258154/pexels-photo-258154.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
      'https://images.pexels.com/photos/271639/pexels-photo-271639.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      'https://images.pexels.com/photos/164595/pexels-photo-164595.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      'https://images.pexels.com/photos/573552/pexels-photo-573552.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      'https://images.pexels.com/photos/271624/pexels-photo-271624.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
    ];
    return Builder(
      builder: (context) {
        final double height = MediaQuery.of(context).size.height * 0.30;
        return CarouselSlider(
          options: CarouselOptions(
            height: height,
            viewportFraction: 1.0,
            enableInfiniteScroll: true, autoPlay: true,
            enlargeCenterPage: false,
            // autoPlay: false,
          ),
          items: dummyUrls
              .map(
                (item) => Container(
                  height: height,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.fitWidth,
                      image: NetworkImage(
                        //  config['image'],
                        item,
                      ),
                      // image: AssetImage('assets/images/home_banner.jpg'),
                    ),
                  ),
                ),
              )
              .toList(),
        );
      },
    );
  }

  Widget gridSection = Expanded(
    flex: 1,
    child: GridView.count(
        crossAxisCount: 3,
        childAspectRatio: 1.0,
        shrinkWrap: true,
        mainAxisSpacing: 2.0,
        crossAxisSpacing: 2.0,
        padding: const EdgeInsets.all(4.0),
        children: <String>[
          'https://images.pexels.com/photos/258154/pexels-photo-258154.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
          'https://images.pexels.com/photos/271639/pexels-photo-271639.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
          'https://images.pexels.com/photos/164595/pexels-photo-164595.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
          'https://images.pexels.com/photos/573552/pexels-photo-573552.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
          'https://images.pexels.com/photos/271624/pexels-photo-271624.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
          'https://images.pexels.com/photos/258154/pexels-photo-258154.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
          'https://images.pexels.com/photos/271639/pexels-photo-271639.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
          'https://images.pexels.com/photos/164595/pexels-photo-164595.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
          'https://images.pexels.com/photos/573552/pexels-photo-573552.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
          'https://images.pexels.com/photos/271624/pexels-photo-271624.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
        ].map((String url) {
          return GridTile(child: Image.network(url, fit: BoxFit.cover));
        }).toList()),
  );
}
