import 'package:flutter/material.dart';
import 'package:makeinindiatrade/src/constant/constant.dart';
import 'package:makeinindiatrade/src/constant/strings.dart';
class SettingPage extends StatelessWidget {
  const SettingPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: Image.asset('assets/images/ic_back_white.png',height: 20,width: 20,)),
          backgroundColor: AppColor.appColor,
          centerTitle: true,
          title: Text(Strings.setting,style: TextStyle(color: Colors.white,fontWeight: FontWeight.w400,fontSize: 16.5),),
      ),
      body: Column(
        children: [
          Expanded(child: 
          ListView(
            children: [
              SizedBox(height: 10,),
              GestureDetector(
                onTap: (){

                },
                              child: Container(
                          
                              height: 45,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  SizedBox(width: 20,),
                                  Image.asset('assets/images/ic_edit_profile.png',height: 20,width: 20,),
                                  SizedBox(width: 20,),
                                  Text('Edit Profile',style: TextStyle(color:Colors.black,fontSize: 15),)
                                ],
                              ),

                            ),
              ),

Divider(color: Colors.grey,),
                           GestureDetector(
                             onTap: (){

                             },
                             child: Container(
                                         color:selectedMenu ==5 ? Color(0xffECE4FE) : Colors.transparent,
                              height: 45,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  SizedBox(width: 20,),
                                  Image.asset('assets/images/ic_change_password.png',height: 20,width: 20,),
                                  SizedBox(width: 20,),
                                  Text('Change Password',style: TextStyle(color:Colors.black,fontSize: 15),)
                                ],
                              ),

                          ),
                           ),
Divider(color: Colors.grey,),
                           GestureDetector(
                             onTap: (){
                               
                             },
                             child: Container(
                                                      color:selectedMenu ==5 ? Color(0xffECE4FE) : Colors.transparent,
                              height: 45,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  SizedBox(width: 20,),
                                  Image.asset('assets/images/ic_notification.png',height: 20,width: 20,),
                                  SizedBox(width: 20,),
                                  Text('Notification Setting',style: TextStyle(color:Colors.black,fontSize: 15),)
                                ],
                              ),

                          ),
                           ),
                         Divider(color: Colors.grey,),
            ],
          ) 
          // ListView.separated(itemBuilder: (context,int index){
          //   return Row(
          //     children: [
          //         SizedBox(width: 20,),

          //         SizedBox(width: 20,),
          //     ],
          //   );
          // }, separatorBuilder: (context, index) => Divider(
          //   color: Colors.black,
          // ), itemCount: 3)
          
          ),
        ],
      ),
       floatingActionButton: FloatingActionButton(onPressed: (){

      },
      backgroundColor: AppColor.appColor,
      child: Image.asset('assets/images/btn_inquiry.png',fit: BoxFit.cover,filterQuality: FilterQuality.high,),
      ),
    );
  }
}