import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:makeinindiatrade/src/model/login_model.dart';

import 'api_list.dart';

class LoginApi {
  Future<LoginModel> loginApi(String userName, String password) async {
    dynamic responseJson;
    await http.post(Apis.LOG_IN, body: {
      'username': userName,
      'password': password,
    }).then((response) {
      responseJson = _returnResponse(response);
    }).catchError((onError) {
      print(onError);
    });
    return LoginModel.fromJson(responseJson);
  }

  dynamic _returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        final responseJson = json.decode(response.body.toString());
        print(responseJson);

        return responseJson;
      case 400:
        throw Exception(response.body.toString());
      case 401:
        throw Exception(response.body.toString());
      case 403:
        throw Exception(response.body.toString());
      case 500:
      default:
        throw Exception(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}
