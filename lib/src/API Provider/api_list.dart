class Apis {
  static const String _baseUrl = 'https://www.makeinindiatrade.com/api';

  static const String LOG_IN = '$_baseUrl/login';
  static const String GET_CATEGORY = '$_baseUrl/category';
}
