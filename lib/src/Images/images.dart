class Images {
  static const String LOGO = 'assets/images/logo.png';
  static const String VISIBLE = 'assets/images/ic_visible.png';
  static const String INVISIBLE = 'assets/images/ic_invisible.png';
  static const String CITYIMAGE = 'assets/images/img_city.png';
  static const String SEARCHICON = 'assets/images/ic_search_white.png';
  static const String SIDEMENU = 'assets/images/ic_menu.png';


    static const String agriculture = 'assets/images/ic_agriculture.png';
    static const String clothes = 'assets/images/ic_apparels_clothes_garments.png';
    static const String ic_architecture_materials_services = 'assets/images/ic_architecture_materials_services.png';
    static const String ic_audit_services_business = 'assets/images/ic_audit_services_business.png';
    static const String ic_automobile = 'assets/images/ic_automobile.png';
    static const String ic_bags_cases = 'assets/images/ic_bags_cases.png';
    static const String ic_bicycle_spares_parts = 'assets/images/ic_bicycle_spares_parts.png';
    static const String ic_books_stationery = 'assets/images/ic_books_stationery.png';
    static const String ic_bpo_call_center_services = 'assets/images/ic_bpo_call_center_services.png';
    // static const String agriculture = 'assets/images/ic_agriculture.png';
    // static const String agriculture = 'assets/images/ic_agriculture.png';
    // static const String agriculture = 'assets/images/ic_agriculture.png';
    // static const String agriculture = 'assets/images/ic_agriculture.png';
    // static const String agriculture = 'assets/images/ic_agriculture.png';
    // static const String agriculture = 'assets/images/ic_agriculture.png';
  
}