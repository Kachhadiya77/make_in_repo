import 'package:flutter/material.dart';
import 'package:makeinindiatrade/src/app.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences.getInstance().then(
    (prefs) => runApp(
      MyApp(prefs),
    ),
  );
}
